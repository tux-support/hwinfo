@extends('layouts.app')

@section('title', 'Users')

@section('head')
@parent

<style>
    #userlistcontainer .panel-block {
        width: 100%;
        flex-grow: 1;
    }


</style>
@endsection

@section('content')


<div id="userlistcontainer" class="panel">
    <h2 class="panel-heading is-3">Users</h2>

        <div class="panel-block">
            <div class="control has-icons-left">
                <input type="text" class="input" placeholder="Search user">
                <span class="icon is-left">
                    <i class="fas fa-search" aria-hidden="true"></i>
                </span>
            </div>
        </div>
            @foreach($users as $user)
            <a href="{{ route('users.show', ['user' => $user->uuid]) }}" class="panel-block">
                <span class="panel-icon">
                    <i class="fas fa-book" aria-hidden="true"></i>
                  </span>
                  {{ $user->name }}
            </a>
            @endforeach


</div>
@endsection
