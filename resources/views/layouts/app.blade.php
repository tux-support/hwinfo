<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @section('head')
        <link rel="stylesheet" href="/css/all.css">
        <link rel="stylesheet" href="/css/app.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js">
    </script>

    @show

</head>

<body>
    @section('navigation')
    <nav class="tabs is-centered">
        <ul>
            <li class="is-active"><a href="/">Submit report</a></li>
            <li class=""><a href="#">Registration</a></li>
            <li class=""><a href="#">Statistics</a></li>
            <li class=""><a href="#">About</a></li>
        </ul>
    </nav>
    @show

    <div class="container">
        @yield('content')
    </div>
</body>

</html>
