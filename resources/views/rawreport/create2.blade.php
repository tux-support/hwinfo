@extends('layouts.app')

@section('title', 'Submit command output')

@section('head')
    @parent

    <style>
        .panel {
            min-width: 350px;
        }

        #main-form-elements fieldset,
        #error-list li {
            flex-grow: 1;
            width: 100%;
        }

        .field {
            width: 100%;
        }

        #commandinfo {
            padding: 1ex;
        }

    </style>

    <script>
        @php
        $arrCommands = array();
        foreach($commandgroups as $commandGroup) {
            foreach($commandGroup->commands as $command) {
                $arrCommands[$command->uid] = $command;
            }
        }
        @endphp
        var commands = @json($arrCommands);

        window.onload = function() {
            let commandSelect = document.getElementById("command");

            // Preselect the command selector bases on the URI fragment (the bit after the hash sign)
            if(window.location.hash) {
                console.log(window.location.hash);
                let hash = window.location.hash.slice(1);
                console.log({'hash': hash})
                if(commands.hasOwnProperty(hash)) {
                    commandSelect.value = hash;
                }
            }

            // Update command description and URI fragment when command selector changes
            commandSelect.addEventListener("change", function(){
                if(commands.hasOwnProperty(commandSelect.value)) {
                    let command = commands[commandSelect.value]
                    document.getElementById("commandinfo").innerHTML = '<p>' + command.description + '<p><code>$ ' + command.cmd + '</code>';
                    window.location.hash = '#' + commandSelect.value;
                } else {
                    document.getElementById("commandinfo").innerHTML = '';
                }
            })
        };
    </script>
@endsection

@section('content')

    <form method="POST" action="/" enctype="multipart/form-data">
        @csrf
        <div class="container">
            <h1 class="title is-1">Submit command output</h1>
            @if ($errors->any())
            <div class="help is-danger is-invalid has-icons-right">
                <h2 class="subtitle is-6">There were errors in your submission:
                </h2>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li><i class="fas fa-exclamation-triangle"></i> {{ $error }}
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <section class="section columns">
            <div class="column is-two-thirds">
                <div class="panel">
                    <h2 class="panel-heading is-primary">
                        Report submission
                    </h2>
                    <div id="main-form-elements" class="panel-block"
                        style="flex-direction: column;">
                        <fieldset>
                            <div class="field">
                                <label for="command"
                                    class="label">Command</label>
                                <div class="control has-icons-left">
                                    <div
                                        class="select @error('command') is-invalid is-danger @enderror">
                                        <select id="command" name="command" autofocus="autofocus">
                                            <option value="no_command"
                                                @if(old('command')=='no_command'
                                                ) selected="selected" @endif>
                                                Please
                                                select
                                                an
                                                option
                                            </option>
                                            @foreach ($commandgroups as
                                            $commandgroup)
                                            <optgroup
                                                label="{{ $commandgroup->title }}">
                                                @foreach ($commandgroup->commands as $command)
                                                <option value="{{ $command->uid }}"
                                                    id="command_{{ $command->uid }}"
                                                    @if(old('command')==$command->uid)
                                                    selected="selected" @endif>
                                                    {{ $command->title }}
                                                </option>
                                                @endforeach
                                            </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="icon is-small is-left">
                                        <i class="fas fa-terminal"></i>
                                    </div>
                                    @error('command')
                                    <div class="help is-danger">
                                        {{ $message }} <i class="fas fa-exclamation-triangle"></i>
                                    </div>
                                    @enderror
                                    <div id="commandinfo">
                                    </div>
                                </div>
                            </div>

                            <div class="field">



                                <div class="tabbed-layout">
                                    <input name="nav" type="radio" class="nav paste-radio" id="paste" checked="checked" />
                                    <div class="page paste-page">
                                      <div class="page-contents">

                                        <div class="field"></div>
                                        <div class="field">
                                            <label for="data" class="label">Command output</label>
                                            <div class="control">
                                                <textarea id="data" name="data"
                                                    placeholder="Enter the output of the command here"
                                                    class="textarea @error('data') is-invalid is-danger @enderror">{{ old('data') }}</textarea>
                                                @error('data')
                                                <div class="help is-danger has-icons-right">
                                                    {{ $message }} <i class="fas fa-exclamation-triangle"></i>
                                                </div>
                                                @enderror
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                    <label class="nav" for="paste">
                                      <span>
                                        Paste Output
                                      </span>
                                    </label>
                                  
                                    <input name="nav" type="radio" class="about-radio" id="about" />
                                    <div class="page about-page">
                                      <div class="page-contents">
                                        <div class="file" id="file-div">
                                            <label class="file-label">
                                              <input class="file-input" type="file" name="resume">
                                              <span class="file-cta">
                                                <span class="file-icon">
                                                  <i class="fas fa-upload"></i>
                                                </span>
                                                <span class="file-label">
                                                  Choose a file…
                                                </span>
                                              </span>
                                              <span class="file-name" id="file-upload-name" style="display: none;">
                                              </span>
                                            </label>
                                          </div>
                                          <script>
                                            document.getElementById('file-div').setAttribute('class', 'file has-name is-fullwidth');
                                            document.getElementById('file-upload-name').setAttribute('style', 'display: auto');
                                          </script>
                                      </div>
                                    </div>
                                    <label class="nav" for="about">
                                      <span>
                                        <i class="fas fa-upload"></i>
                                        Upload file
                                        </span>
                                    </label>
                                    <div class="nav"></div>
                                  </div>

                            </div>



<style>


.tabbed-layout label.nav, .tabbed-layout a {
  cursor: pointer;
  user-select: none;
  text-decoration: none;
  display: inline-block;
  color: inherit;
  transition: border 0.2s;
  border-bottom: 5px solid rgba(142, 68, 173, 0.2);
  padding: 3px 2px;
}

tabbed-layout .nav {

}


.tabbed-layout label.nav:hover, .tabbed-layout a:hover {
  border-bottom-color: #9b59b6;
}

.tabbed-layout {
  display: grid;
  height: 100%;
  width: 100%;
  overflow: hidden;
  grid-template-rows: 50px 1fr;
  grid-template-columns: 1fr 1fr 1fr;
}

.tabbed-layout input[type="radio"] {
  display: none;
}

.tabbed-layout label.nav, .tabbed-layout .nav {
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  /*
  border-bottom: 2px solid #8e44ad;
  background: #ecf0f1;
  */
  border-bottom: 1px solid #dbdbdb;
  user-select: none;
  transition: background 0.4s, padding-left 0.2s;
  padding-left: 0;
}

.tabbed-layout input[type="radio"]:checked + .page + label.nav {
/*
  background: #9b59b6;
  color: #ffffff;
  */
  border-bottom: 1px solid #3273dc;
  padding-left: 20px;
}
input[type="radio"]:checked + .page + .tabbed-layout label.nav span {
  padding-left: 20px;
}
input[type="radio"]:checked + .page + .tabbed-layout label.nav svg {
  opacity: 1;
}
.tabbed-layout label.nav span {
  padding-left: 0px;
  position: relative;
}
.tabbed-layout label.nav svg {
  left: 0;
  top: -3px;
  position: absolute;
  width: 15px;
  opacity: 0;
  transition: opacity 0.2s;
}

.tabbed-layout .page {
  grid-column-start: 1;
  grid-row-start: 2;
  grid-column-end: span 3;
  /*padding: 0px 20px;*/
  display: flex;
  align-items: center;
}

.tabbed-layout .page-contents > * {
  opacity: 0;
  transform: translateY(20px);
  transition: opacity 0.2s, transform 0.2s;
}
.tabbed-layout .page-contents > *:nth-child(1) {
  transition-delay: 0.4s;
}
.tabbed-layout .page-contents > *:nth-child(2) {
  transition-delay: 0.6s;
}
.tabbed-layout .page-contents > *:nth-child(3) {
  transition-delay: 0.8s;
}
.tabbed-layout .page-contents > *:nth-child(4) {
  transition-delay: 1s;
}
.tabbed-layout .page-contents > *:nth-child(5) {
  transition-delay: 1.2s;
}
.tabbed-layout .page-contents > *:nth-child(6) {
  transition-delay: 1.4s;
}
.tabbed-layout .page-contents > *:nth-child(7) {
  transition-delay: 1.6s;
}
.tabbed-layout .page-contents > *:nth-child(8) {
  transition-delay: 1.8s;
}
.tabbed-layout .page-contents > *:nth-child(9) {
  transition-delay: 2s;
}
.tabbed-layout .page-contents > *:nth-child(10) {
  transition-delay: 2.2s;
}
.tabbed-layout .page-contents > *:nth-child(11) {
  transition-delay: 2.4s;
}
.tabbed-layout .page-contents > *:nth-child(12) {
  transition-delay: 2.6s;
}
.tabbed-layout .page-contents > *:nth-child(13) {
  transition-delay: 2.8s;
}
.tabbed-layout .page-contents > *:nth-child(14) {
  transition-delay: 3s;
}
.tabbed-layout .page-contents > *:nth-child(15) {
  transition-delay: 3.2s;
}
.tabbed-layout .page-contents > *:nth-child(16) {
  transition-delay: 3.4s;
}
.tabbed-layout .page-contents > *:nth-child(17) {
  transition-delay: 3.6s;
}
.tabbed-layout .page-contents > *:nth-child(18) {
  transition-delay: 3.8s;
}
.tabbed-layout .page-contents > *:nth-child(19) {
  transition-delay: 4s;
}
.tabbed-layout .page-contents > *:nth-child(20) {
  transition-delay: 4.2s;
}

.tabbed-layout input[type="radio"] + .page {
  transition: transform 0.2s;
  transform: translateX(100%);
}

.tabbed-layout input[type="radio"]:checked + .page {
  transform: translateX(0%);
}
.tabbed-layout input[type="radio"]:checked + .page .page-contents > * {
  opacity: 1;
  transform: translateY(0px);
}

.tabbed-layout .page-contents {
  max-width: 100%;
  width: 100%;
  margin: 0 auto;
}


</style>

                        </fieldset>
                        <fieldset>
                            <div class="field"></div>
                            <div class="field is-grouped">
                                <div class="control">
                                    <input type="submit"
                                        class="button is-link" />
                                </div>
                                <div class="control">
                                    <input type="reset"
                                        class="button is-link is-light" />
                                </div>
                            </div>
                        </fieldset>
                    </div>

                </div>
            </div>
            <div class="column is-one-third">
                <div class="panel">
                    <p class="panel-heading is-primary">
                        Privacy
                    </p>

                    <div class="panel-block">
                        <div class="field">
                            <div class="control">
                                <label for="exclude_statistics" class="label">
                                    <input type="checkbox"
                                        id="exclude_statistics"
                                        name="exclude_statistics" value="true"
                                        @if(old('exclude_statistics'))
                                        checked="checked" @endif
                                        class="checkbox" />
                                    Exclude me from statistics
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="panel-block">
                        <div class="field">
                            <label for="public_listing" class="label">Public
                                listing</label>
                            <div class="control">
                                <div class="select">
                                    <select id="public_listing"
                                        name="public_listing">
                                        <option value="private"
                                            @if(old('public_listing')=='private'
                                            ) selected="selected" @endif>Keep
                                            data
                                            private
                                        </option>
                                        <option value="anonymized"
                                            @if(old('public_listing')=='anonymized'
                                            ) selected="selected" @endif>Only
                                            list
                                            anonymous
                                            and
                                            safe
                                            data</option>
                                        <option value="public"
                                            @if(old('public_listing')=='public'
                                            ) selected="selected" @endif>
                                            Publicly list
                                            my
                                            report
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="panel">
                    <p class="panel-heading is-primary">
                        Registration
                    </p>
                    <div class="panel-block">
                        <div class="field">
                            <label for="key" class="label">Secret key <span
                                    class="sidenote">(leave
                                    blank
                                    for autogeneration)</span></label>
                            <div class="control has-icons-left">
                                <input type="text" name="key"
                                    class="input @error('key') is-invalid is-danger @enderror"
                                    value="{{ old('key') }}" />
                                <span class="icon is-small is-left">
                                    <i class="fas fa-lock"></i>
                                </span>
                                @error('key')
                                <div class="help is-danger">
                                    {{ $message }} <i
                                        class="fas fa-exclamation-triangle"></i>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="panel-block">
                        <div class="field">
                            <label for="hwprofile" class="label">Hardware
                                Profile ID
                                <span class="sidenote">(optional)</span></label>
                            <div class="control has-icons-left">
                                <input type="text" name="hwprofile"
                                    id="hwprofile"
                                    placeholder="xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
                                    class="input @error('hwprofile') is-invalid is-danger @enderror"
                                    value="{{ old('hwprofile') }}" />
                                <span class="icon is-small is-left">
                                    <i class="fas fa-lock"></i>
                                </span>
                                @error('hwprofile')
                                <div class="help is-danger">
                                    {{ $message }} <i
                                        class="fas fa-exclamation-triangle"></i>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="panel-block">
                        <div class="field ">
                            <label for="campaign" class="label">Campaign ID
                                <span class="sidenote">(optional)</span></label>
                            <div class="control has-icons-left has-icons-right">
                                <input type="text" name="campaign" id="campaign"
                                    placeholder="xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
                                    class="input @error('campaign') is-invalid is-danger @enderror"
                                    value="{{ old('campaign') }}" />
                                <span class="icon is-small is-left">
                                    <i class="fas fa-lock"></i>
                                </span>
                                @error('campaign')
                                <div class="help is-danger">
                                    {{ $message }} <i
                                        class="fas fa-exclamation-triangle"></i>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </form>

@endsection