<html>
    <head>
        <title>Report submitted</title>
        <style>
            label, input[type=submit] {
                display: block;
                margin-top: 1rem;
                padding: 0;
                font-size: large;
            }
            input, textarea, select {
                width: 300px;
            }
            .sidenote {
                font-size: small;
            }
        </style>
    </head>
    <body>
        <h1>Report submitted</h1>
        <pre>
            @php
                print_r($rawreport);
            @endphp
        </pre>
    </body>
</html>