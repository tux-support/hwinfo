Hwinfo
===
Hwinfo is a supposed to be a smarter version of pastebin specifically for submitting output from hwinfo and other system information tools.

The idea is that if you need to get help from the community or you just feel like sharing information about your system for any other reason, you submit the required reports and then share your Hardware profile by a URL.

Hwinfo consists of three parts: Reports, Hardware Profiles and Campaigns.

## Reports
A report is any type of output, whether hwinfo understands it or not, but the most useful types will be the ones that hwinfo can use to populate your hardware profile.

## Hardware Profiles
Multiple reports can be submitted and collated under a single profile. You can then share this profile whereever needed.

## Campaigns
A campaign is simply a collection of hardware profiles.
Developers, community managers or others might be interested in knowing the most common types of systems among their users and can easily survey the users by sharing a link with a campaign ID.

## Users
It is not necessary to have a user account to use any of the features of hwinfo, but it makes it easier to manage your data, since you can list and manage your hardware profiles and campaigns without remembering UUIDs or secret keys.


# Usage

## Submitting reports
Data can be published using the web form or submitted using cURL from the command line.

JSON encoded output is preferred.

### Examples of submissions
In its simplest form, a submission is made by piping the output of a command to cURL.
```
$ lsblk --json  | curl https://hwinfo.io -d @-
```
This will give you back a UUID for your submission, a URL to see your submission and a secret key, that is required if you ever need to modify or delete your report.

Sending the full command alongside the data helps hwinfo understand your report better. This can be accomplished like so:
```
cmd="lsblk --json" bash -c '$cmd | curl https://hwinfo.io -F "cmd=$cmd" -F "data=@-" 
```
First we assign the command to a variable, then pass it on to bash and finally we pipe it to cURL.<br>
Note that we are sending the form fields `cmd` and `data`. The first contains the exact command executed and the latter is the output.

## UUIDs and Secret keys
Hwinfo is built to not require any user accounts, but in order to make it possible to modify or delete your content, a long string looking like 72043364-4cf5-4a05-8f08-4b1659b3b40e called a [Universally unique identifier or UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier) is generated to uniquely identify each submission. In addition, a string of random characters is generated as a secret key, that you will need if you ever wish to modify or delete your submission.

A very simply user account system has been implemented to alleviate the need to remember your UUIDs and keys, but it is completely optional.

## HTTP POST fields
- exclude_statistics
  - true: No part of your report data will be used for statistical purposes.
  - false (default): Certain non-sensitive parts of the reports will be used for statistical purposes. See the privacy section for detauls.
- public_listing
  - private (default): Your data is not listed anywhere.
  - anonymized: Only information known to be safe will be publicly listed.
  - public: List all data from your reports publicly. *WARNING: THIS MAY LEAK SENSITIVE DATA* 


# Privacy
By default, certain non-sensitive information about each system is stored for statistical purposes.
This information includes:
- CPU
  - Manufacturer
  - Model
  - Clock frequency
- Memory
  - Amount
  - Clock frequency
- Motherboard
  - DMI/SMBIOS information
  - PCI devices
- Network
  - Number of devices
  - Manufacturer
  - Model
- Storage
  - Number of drives
  - Drive capacity
  - Manufacturer
  - Model
- Graphics
  - Manufacturer
  - Model
  - Drivers in use
- Displays
  - Number of displays
  - Manufacturer
  - Model
  - Configuration
- Operating system
  - Kernel version
- Input devices
  - Manufacturer
  - Model
- Geolocation based on IP address

You can choose not to share this information or you can even choose to share your complete reports.

We log the IP address and the HTTP headers along with each report. The headers are for development and operational purposes and the IP address is for statistics and abuse prevention.

# Development
Build using [Laravel](https://laravel.com/).

The CSS framework is [Bulma](https://bulma.io/).

The tab layout is a modified version of the source [code](https://codepen.io/EightArmsHQ/pen/rNBZKNZ) from [eightarmshq](https://codepen.io/EightArmsHQ).
