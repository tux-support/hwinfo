<?php

namespace App\Http\Controllers;

use App\HwprofileRawreport;
use Illuminate\Http\Request;

class HwprofileRawreportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HwprofileRawreport  $hwprofileRawreport
     * @return \Illuminate\Http\Response
     */
    public function show(HwprofileRawreport $hwprofileRawreport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HwprofileRawreport  $hwprofileRawreport
     * @return \Illuminate\Http\Response
     */
    public function edit(HwprofileRawreport $hwprofileRawreport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HwprofileRawreport  $hwprofileRawreport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HwprofileRawreport $hwprofileRawreport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HwprofileRawreport  $hwprofileRawreport
     * @return \Illuminate\Http\Response
     */
    public function destroy(HwprofileRawreport $hwprofileRawreport)
    {
        //
    }
}
