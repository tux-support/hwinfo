<?php

namespace App\Http\Controllers;

use App\Hwprofile;
use Illuminate\Http\Request;

class HwprofileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hwprofile  $hwprofile
     * @return \Illuminate\Http\Response
     */
    public function show(Hwprofile $hwprofile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hwprofile  $hwprofile
     * @return \Illuminate\Http\Response
     */
    public function edit(Hwprofile $hwprofile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hwprofile  $hwprofile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hwprofile $hwprofile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hwprofile  $hwprofile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hwprofile $hwprofile)
    {
        //
    }
}
