<?php

namespace App\Http\Controllers;

use App\CommandCategory;
use App\Rawreport;
use Illuminate\Http\Request;

class RawreportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commandgroups = CommandCategory::all();
        return response()->view('rawreport.create', [
            'commandgroups' => $commandgroups
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'command' => ['required', 'max:255', 'not_in:no_command'],
            'data' => ['required'],
            'key' => ['nullable', 'min:10', 'max:20'],
            'hwprofile' => ['nullable', 'uuid'],
            'campaign' => ['nullable', 'uuid'],
        ]);
        $rawreport = new Rawreport;
        $rawreport->command = $request->input('command');
        $rawreport->ip = $request->ip();
        $rawreport->httpheaders = json_encode(getallheaders());
        if ($request->hasFile('data') && $request->file('data')->isValid()) {
            $rawreport->data = $request->file('data');
        } else {
            $rawreport->data = $request->input('data');
        }
        $rawreport->public_listing = $request->input('public_listing', false);
        $rawreport->no_statistics = $request->input('prevent_statistics', true);

        return response()->view('rawreport.store', [
            'request' => $request,
            'rawreport' => $rawreport->toArray(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rawreport  $rawreport
     * @return \Illuminate\Http\Response
     */
    public function show(Rawreport $rawreport)
    {
        return response()->view('response', [
            'rawreport' => $rawreport,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rawreport  $rawreport
     * @return \Illuminate\Http\Response
     */
    public function edit(Rawreport $rawreport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rawreport  $rawreport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rawreport $rawreport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rawreport  $rawreport
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rawreport $rawreport)
    {
        //
    }
}
