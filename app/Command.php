<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    public function rawreport() {
        return $this->hasMany('App\Rawreport');
    }

    public function commandcategory()
    {
        return $this->belongsTo('App\CommandCategory');
    }
}
