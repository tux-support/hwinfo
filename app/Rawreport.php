<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rawreport extends Model
{

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id'
    ];
    
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function command() {
        return $this->hasOne('App\Command');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function hwprofile()
    {
        return $this->belongsToMany('App\Hwprofile', 'hwprofile_rawreports');
    }
}
