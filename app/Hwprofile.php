<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hwprofile extends Model
{
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function rawreports() {
        return $this->belongsToMany('App\Rawreport', 'hwprofile_rawreports');
    }
}
