<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function hwprofiles()
    {
        return $this->hasMany('App\Hwprofile');
    }
}
