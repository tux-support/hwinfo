<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommandCategory extends Model
{
    public function commands()
    {
        return $this->hasMany('App\Command');
    }

}
