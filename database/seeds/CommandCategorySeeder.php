<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommandCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('command_categories')->insert([
            [
                'title' => 'Unlisted command',
                'order' => 100
            ],
            [
                'title' => 'General System Information',
                'order' => 101
            ],
            [
                'title' => 'CPU Information',
                'order' => 102
            ],
            [
                'title' => 'Network Information',
                'order' => 103
            ],
            [
                'title' => 'Graphics Information',
                'order' => 104
            ]
        ]);
    }
}
