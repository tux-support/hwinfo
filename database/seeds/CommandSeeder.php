<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $commandsGrouped = [
            1 => [
                'other_command' => [
                    'title' => 'Unlisted command (generic)',
                    'description' => 'Output of any unsupported command.',
                    'cmd' => '[any command]'
                ],
                'other_command_json' => [
                    'title' => 'Unlisted command (JSON)',
                    'description' => 'Output of any unsupported command that returns JSON.',
                    'cmd' => '[any json command]'
                ],
                'other_command_xml' => [
                    'title' => 'Unlisted command (XML)',
                    'description' => 'Output of any unsupported command that returns XML.',
                    'cmd' => '[any xml command]'
                ],
                'other_command_yaml' => [
                    'title' => 'Unlisted command (YAML)',
                    'description' => 'Output of any unsupported command that returns YAML.',
                    'cmd' => '[any yaml command]'
                ],
            ],
            2 => [
                'linux_lshw' => [
                    'title' => 'lshw - List hardware',
                    'description' => 'List hardware on the system.</br>For most information, use root permissions when executing command.',
                    'cmd' => 'sudo lshw -json',
                ],
                'linux_lsblk' => [
                    'title' => 'lsblk - List block devices',
                    'description' => 'List block devices (harddrives) on your system.<br/>Does not require root permisisons.',
                    'cmd' => 'lsblk -e7 --json'
                ],
                'linux_lsusb' => [
                    'title' => 'lsusb - List USB devices',
                    'description' => 'List USB devices on the system.<br/>Does not require root permisisons.',
                    'cmd' => 'lsusb'
                ],
                'dmidecode' => [
                    'title' => 'dmidecode - List DMI/SMBIOS content',
                    'description' => 'Lists information about your computer by looking at the DMI/SMBIOS table.',
                    'cmd' => 'dmidecode'
                ],
                'lspci' => [
                    'title' => 'lspci - List PCI information',
                    'description' => 'Lists information about your PCI devices.
                    You might want to update your PCI database first, with ```sudo update-pciids```',
                    'cmd' => 'lspci -mm'
                ],
                'steam_system_information' => [
                    'title' => "Steam System information",
                    'description' => "System information from the Help menu in Steam",
                    'cmd' => "Go to Steam -> Help -> System Information. Wait until it is done loading, then select the text and copy it."
                ]
            ],
            3 => [
                'lscpu' => [
                    'title' => 'lscpu - List CPU information (preferred)',
                    'description' => 'Lists information about your CPU. This is preferred over /proc/cpuinfo.',
                    'cmd' => 'lscpu --json'
                ],
                'lscpu_extended' => [
                    'title' => 'lscpu extended - List CPU cores',
                    'description' => 'Lists information about each CPU core',
                    'cmd' => 'lscpu --extended --json'
                ],
                'cpuinfo' => [
                    'title' => '/proc/cpuinfo - List CPU information',
                    'description' => 'Lists information about your CPU.',
                    'cmd' => 'cat /proc/cpuinfo'
                ],
            ],
            4 => [
                'lshw_network' => [
                    'title' => 'lshw -C network - List network devices',
                    'description' => 'List network devices.',
                    'cmd' => 'lshw -C network -json'
                ],
                'ip_a' => [
                    'title' => 'ip address - List IP details',
                    'description' => 'List detailed IP addresses and settings.',
                    'cmd' => 'ip -details -json address'
                ],
                'ip_route' => [
                    'title' => 'ip route - List IP routes',
                    'description' => 'List information about the routes configured on your system.',
                    'cmd' => 'ip -json route'
                ],
                'ifconfig' => [
                    'title' => 'ifconfig - List status of all network interfaces',
                    'description' => 'List status of all network interfaces.',
                    'cmd' => 'ifconfig -a'
                ],
                'iptables' => [
                    'title' => 'iptables - List packet filter rules',
                    'description' => 'List all packet filter rules configured on your system.',
                    'cmd' => 'iptables --list'
                ],
                'ufw_status' => [
                    'title' => 'ufw status - Firewall status',
                    'description' => 'List firewall rules on your system.',
                    'cmd' => 'sudo ufw status verbose'
                ],

                
            ],
            5 => [
                'glxinfo' => [
                    'title' => 'glxinfo - List GPU information',
                    'description' => 'Lists information about your GPU.',
                    'cmd' => 'glxinfo'
                ],
                'nvidia-smi' => [
                    'title' => 'nvidia-smi - List nVidia GPU Information',
                    'description' => 'Lists information about your nVidia device.',
                    'cmd' => 'nvidia-smi -x'
                ]
            ]
        ];
        $commandsToInsert = [];
        foreach($commandsGrouped as $groupId => $commands) {
            foreach($commands as $commandId => $command) {
                $command['uid'] = $commandId;
                $command['command_category_id'] = $groupId;
                $commandsToInsert[] = $command;
            }
        }
        DB::table('commands')->insert($commandsToInsert);
    }
}
