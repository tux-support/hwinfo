<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(CommandCategorySeeder::class);
        $this->call(CommandSeeder::class);
        $this->call(CampaignSeeder::class);
        $this->call(HwprofileSeeder::class);
        $this->call(RawreportSeeder::class);
        $this->call(HwprofileRawreportSeeder::class);
        
    }
}
