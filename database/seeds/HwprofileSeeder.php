<?php

use App\Hwprofile;
use Illuminate\Database\Seeder;

class HwprofileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Hwprofile::class, 150)->create();
    }
}
