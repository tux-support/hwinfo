<?php

use App\Rawreport;
use Illuminate\Database\Seeder;

class RawreportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Rawreport::class, 100)->create();
    }
}
