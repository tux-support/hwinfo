<?php

use App\HwprofileRawreport;
use Illuminate\Database\Seeder;

class HwprofileRawreportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(HwprofileRawreport::class, 150)->create();
    }
}
