<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRawreportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rawreports', function (Blueprint $table) {
            $table->id('id')->unique();
            $table->uuid('uuid')->unique();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->string('key', 50);
            $table->ipAddress('ip');
            $table->longText('data');
            $table->string('command', 255);
            $table->text('httpheaders');
            
            $table->foreignId('command_id')->constrained();
            $table->foreignId('user_id')->nullable()->constrained();
            $table->foreignId('hwprofile_id')->nullable()->constrained();


            //$table->uuid('hwprofile_id')->nullable();
            //$table->foreign('hwprofile_id')->references('id')->on('hwprofiles');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rawreports');
    }
}
