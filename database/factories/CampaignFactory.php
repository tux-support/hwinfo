<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Campaign;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Campaign::class, function (Faker $faker) {
    return [
        'uuid' => Str::orderedUuid(),
        'key' => Str::random(20),
        'user_id' => User::all()->random()->id,
        'title' => $faker->sentence(3),
        'description' => $faker->paragraph(3),
    ];
});
