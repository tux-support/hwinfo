<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\HwprofileRawreport;
use App\Hwprofile;
use App\Rawreport;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(HwprofileRawreport::class, function (Faker $faker) {
    return [
        'hwprofile_id' => Hwprofile::all()->random()->id,
        'rawreport_id' => Rawreport::all()->random()->id,
    ];
});
