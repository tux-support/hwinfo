<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hwprofile;
use App\User;
use App\Campaign;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Hwprofile::class, function (Faker $faker) {
    return [
        'uuid' => Str::orderedUuid(),
        'key' => Str::random(20),
        'title' => $faker->sentence(5),
        'user_id' => User::all()->random()->id
    ];
});