<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Rawreport;
use App\Command;
use App\Hwprofile;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Rawreport::class, function (Faker $faker) {
    //$faker->addProvider(new Faker\Provider\Internet($faker));
    $command = Command::all()->random();
    return [
        'key' => Str::random(20),
        'ip' => $faker->ipv4(),
        'data' => $faker->paragraphs(3, true),
        'command' => $command->cmd,
        'command_id' => $command->id,
        'uuid' => Str::orderedUuid(),
        'user_id' => rand(0,1)?Command::all()->random()->id:null,
        'hwprofile_id' => rand(0,1)?Hwprofile::all()->random()->id:null,
        'httpheaders' => 'Host: github.githubassets.com
        User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
        Accept: image/webp,*/*
        Accept-Language: en-US,en;q=0.5
        Accept-Encoding: gzip, deflate, br
        DNT: 1
        Connection: keep-alive
        Referer: https://github.com/fzaninotto/Faker
        Pragma: no-cache
        Cache-Control: no-cache
        TE: Trailers',

    ];
});
